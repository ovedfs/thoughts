require('./bootstrap');

window.Vue = require('vue');

Vue.component('mythoughts-component', require('./components/MyThoughtsComponent.vue'));
Vue.component('form-component', require('./components/FormComponent.vue'));
Vue.component('thought-component', require('./components/ThoughtComponent.vue'));

const app = new Vue({
	el: '#app'
});
